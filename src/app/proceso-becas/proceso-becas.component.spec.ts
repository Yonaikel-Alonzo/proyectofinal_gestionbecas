
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcesoBecasComponent } from './proceso-becas.component';

describe('ProcesoBecasComponent', () => {
  let component: ProcesoBecasComponent;
  let fixture: ComponentFixture<ProcesoBecasComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ProcesoBecasComponent]
    });
    fixture = TestBed.createComponent(ProcesoBecasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
