import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registro-sesion',
  templateUrl: './registro-sesion.component.html',
  styleUrls: ['./registro-sesion.component.css','registro-sesion.component1.css']
})
export class RegistroSesionComponent {
  dni: string = '';
  name: string = '';
  apell: string = '';
  email: string = '';
  password: string = '';
  na: string = '';
  cel: string = '';
 

  errorMessage: string = '';
  passwordValid: boolean = true;

  constructor(private router: Router) {}

  Registrarse() {
    if (
      this.name === '' || this.apell === '' || this.email === '' ||
      this.password === '' || this.dni === '' || this.na === '' || this.cel === ''
    ) {
      this.errorMessage = 'Por favor complete todos los campos.';
      alert('Complete todos los campos');
      return false;
    }
  
    if (this.dni.length !== 10) {
      this.errorMessage = 'Por favor ingrese la cédula correctamente.';
      alert('Por favor ingrese la cédula correctamente.');
      return false;
    }
  
    if (!this.email.includes('@live.uleam.edu.ec')) {
      this.errorMessage = 'El correo debe ser institucional (@live.uleam.edu.ec).';
      alert('El correo debe ser institucional');
      return false;
    }
  
    if (!/^(?=.*[a-z])(?=.*[A-Z])(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]/.test(this.password)) {
      alert('La contraseña no cumple con los criterios requeridos.');
      return;
    }
    alert(" Registro exitoso.")
  
       const datosRegistro = {
        dni: this.dni,
        nombre: this.name,
        apellido: this.apell,
        correo: this.email,
        nacionalidad: this.na,
        telefono: this.cel,
        contraseña: this.password
       
      };

      localStorage.setItem('datosRegistro', JSON.stringify(datosRegistro));
    this.router.navigate(["../inicio/"]);
    return true; 
  }
}
