import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrincialSesionComponent } from './princial-sesion.component';

describe('PrincialSesionComponent', () => {
  let component: PrincialSesionComponent;
  let fixture: ComponentFixture<PrincialSesionComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PrincialSesionComponent]
    });
    fixture = TestBed.createComponent(PrincialSesionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
