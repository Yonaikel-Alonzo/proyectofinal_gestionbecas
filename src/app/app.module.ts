import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IniciarSesionComponent } from './iniciar-sesion/iniciar-sesion.component';
import { RegistroSesionComponent } from './registro-sesion/registro-sesion.component';
import { InicioComponent } from './inicio/inicio.component';
import { PostulacionBecasComponent } from './postulacion-becas/postulacion-becas.component';
import { ConsultaBecasComponent } from './consulta-becas/consulta-becas.component';
import { PerfilUsuarioComponent } from './perfil-usuario/perfil-usuario.component';
import { RecuperarContraseComponent } from './recuperar-contrase/recuperar-contrase.component';
import { ProcesoBecasComponent } from './proceso-becas/proceso-becas.component';

import { FormsModule } from '@angular/forms';
import { PrincialSesionComponent } from './princial-sesion/princial-sesion.component';
import { HttpClientModule } from '@angular/common/http';




@NgModule({
  declarations: [
    AppComponent,
    InicioComponent,
    IniciarSesionComponent,
    RegistroSesionComponent,
    PostulacionBecasComponent,
    ConsultaBecasComponent,
    PerfilUsuarioComponent,
    RecuperarContraseComponent,
    ProcesoBecasComponent,
    PrincialSesionComponent
    
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
 
  ],
  providers: [],
  bootstrap: [AppComponent]
  
})
export class AppModule { }
