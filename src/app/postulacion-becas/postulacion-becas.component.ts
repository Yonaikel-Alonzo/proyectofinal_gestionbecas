import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-postulacion-becas',
  templateUrl: './postulacion-becas.component.html',
  styleUrls: ['./postulacion-becas.component.css', './postulacion-becas.component2.css']
})
export class PostulacionBecasComponent {
  formStepsNum: number = 0; // Variable para almacenar el número de paso actual
  totalPasos: number = 3; // Número total de pasos en el formulario

  siguientePaso(): void {
    if (this.formStepsNum < this.totalPasos) {
      this.formStepsNum++;
      
      this.actualizarBarraProgreso();
    }
  }
  anteriorPaso(): void {
    if (this.formStepsNum > 0) {
      this.formStepsNum--;
     
      this.actualizarBarraProgreso();
    }
  }

  actualizarBarraProgreso(): void {
    const progressSteps = document.querySelectorAll(".progress-step");
    progressSteps.forEach((progressStep, index) => {
      if (progressStep instanceof HTMLElement) {
        if (index <= this.formStepsNum) {
          progressStep.classList.add("progress-step-active");
        } else {
          progressStep.classList.remove("progress-step-active");
        }
      }
    });
  
    const progress = document.getElementById("progress");
    if (progress instanceof HTMLElement) {
      const progressActive = document.querySelectorAll(".progress-step-active");
      progress.style.width = `${(progressActive.length - 1) / (this.totalPasos - 1) * 100}%`;
    }
  }
  constructor(private router: Router) {}
  EnviarBeca() {
    alert("Postulacion  Exitosa");
    // Redirigir a la página de registro
    this.router.navigate(["../princial-sesion/"]);
  }
}