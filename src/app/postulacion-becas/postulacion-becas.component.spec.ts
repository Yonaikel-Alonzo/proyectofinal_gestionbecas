import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PostulacionBecasComponent } from './postulacion-becas.component';

describe('PostulacionBecasComponent', () => {
  let component: PostulacionBecasComponent;
  let fixture: ComponentFixture<PostulacionBecasComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PostulacionBecasComponent]
    });
    fixture = TestBed.createComponent(PostulacionBecasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
