import { Component } from '@angular/core';
import { DataService } from '../data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-consulta-becas',
  templateUrl: './consulta-becas.component.html',
  styleUrls: ['./consulta-becas.component.css']
})
export class ConsultaBecasComponent {
  becasData: any[] = [];

  constructor(private dataService: DataService, private router: Router) { }

  consultar(): void {
    this.dataService.getBecasData().subscribe((data: any[]) => {
      this.becasData = data;
      // Almacenar los datos en localStorage después de obtenerlos
      localStorage.setItem('becasData', JSON.stringify(this.becasData));
    });
  }

  Regresar() {
    // Redirigir a la página de registro
    this.router.navigate(["../princial-sesion/"]);
  }
}
