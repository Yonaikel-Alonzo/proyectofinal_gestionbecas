import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultaBecasComponent } from './consulta-becas.component';

describe('ConsultaBecasComponent', () => {
  let component: ConsultaBecasComponent;
  let fixture: ComponentFixture<ConsultaBecasComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ConsultaBecasComponent]
    });
    fixture = TestBed.createComponent(ConsultaBecasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
