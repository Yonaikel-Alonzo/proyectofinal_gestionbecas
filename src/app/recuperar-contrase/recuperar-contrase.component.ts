import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-recuperar-contrase',
  templateUrl: './recuperar-contrase.component.html',
  styleUrls: ['./recuperar-contrase.component.css']
})
export class RecuperarContraseComponent {
  email: string = '';
  
  constructor(private router: Router) {
    // Obtener el correo electrónico guardado previamente, si existe
    const storedEmail = localStorage.getItem('storedEmail');
    if (storedEmail) {
      this.email = storedEmail;
    }
  }

  Recupera() {
    if (!this.emailIsValid(this.email)) {
      alert("Por favor ingrese un correo electrónico válido");
      return;
    }
    
    // Guardar el correo electrónico en localStorage
    localStorage.setItem('Correo', this.email);
    
    // Lógica para enviar el correo de recuperación de contraseña
    alert("Enviando solicitud de recuperación de contraseña.");
    this.router.navigate(["../inicio"]);
    // Lógica adicional si es necesario
  }

  private emailIsValid(email: string): boolean {
    const email_regex = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
    return email_regex.test(email);
  }
}
