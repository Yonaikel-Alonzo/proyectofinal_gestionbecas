import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecuperarContraseComponent } from './recuperar-contrase.component';

describe('RecuperarContraseComponent', () => {
  let component: RecuperarContraseComponent;
  let fixture: ComponentFixture<RecuperarContraseComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [RecuperarContraseComponent]
    });
    fixture = TestBed.createComponent(RecuperarContraseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
