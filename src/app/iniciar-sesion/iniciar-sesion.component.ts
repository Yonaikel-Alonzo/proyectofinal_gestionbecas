import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-iniciar-sesion',
  templateUrl: './iniciar-sesion.component.html',
  styleUrls: ['./iniciar-sesion.component.css']
})
export class IniciarSesionComponent {
  email: string = '';
  password: string = '';
  mensajeError: string = '';

  constructor(private router: Router) {}

  Inicio() {
    if (this.email === '' || this.password === '') {
      this.mensajeError = 'Por favor complete todos los campos.';
      alert('Complete todos los campos');
    } else {
      if (this.email.includes('@live.uleam.edu.ec')) {
        if (this.password.length >= 8) { // Asegura que la contraseña tenga al menos 5 caracteres
          alert('Inicio Exitoso');
          let credenciales: { email: string, password: string }[] = JSON.parse(localStorage.getItem('credenciales') || '[]');
          
          credenciales.push({ email: this.email, password: this.password });
          
          localStorage.setItem('credenciales', JSON.stringify(credenciales));
          
          this.router.navigate(["../princial-sesion/"]);
        } else {
          alert('La contraseña debe tener al menos 5 caracteres');
        }
      } else {
        alert('Debe ser un correo institucional');
      }
    }
  }
  

  Registrar() {
    this.router.navigate(["../registro-sesion/"]);
  }
}
