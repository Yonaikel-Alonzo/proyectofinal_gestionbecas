import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InicioComponent } from './inicio/inicio.component';
import { IniciarSesionComponent } from './iniciar-sesion/iniciar-sesion.component';
import { RegistroSesionComponent } from './registro-sesion/registro-sesion.component';
import { PostulacionBecasComponent } from './postulacion-becas/postulacion-becas.component';
import { ProcesoBecasComponent } from './proceso-becas/proceso-becas.component';
import { ConsultaBecasComponent } from './consulta-becas/consulta-becas.component';
import { PerfilUsuarioComponent } from './perfil-usuario/perfil-usuario.component';
import { RecuperarContraseComponent } from './recuperar-contrase/recuperar-contrase.component';
import { PrincialSesionComponent } from './princial-sesion/princial-sesion.component';

const routes: Routes = [ 
  {path:"", redirectTo: "inicio", pathMatch:"full"},
  {path:"iniciar-sesion",component:IniciarSesionComponent},
  {path:"registro-sesion",component:RegistroSesionComponent},
  {path:"inicio",component: InicioComponent},
  {path: "postulacion-becas",component:PostulacionBecasComponent},
  {path: "proceso-becas",component:ProcesoBecasComponent},
  {path: "consulta-becas",component:ConsultaBecasComponent},
  {path: "perfil-usuario",component:PerfilUsuarioComponent},
  {path: "recuperar-contrase",component:RecuperarContraseComponent},
  {path: "princial-sesion", component:PrincialSesionComponent},
  
  
  {path:"**", redirectTo:"inicio", pathMatch:"full"},
]


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
